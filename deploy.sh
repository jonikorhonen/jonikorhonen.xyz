#! /usr/bin/env sh

set -e

SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")
source="$DIR"/dist/
user=joni
server=jonikorhonen.xyz
target=/var/www/htdocs/jonikorhonen.xyz

# -a rsync archive mode
# -P don't set directory timestamps
# --delete delete files from target server not found from source
rsync -a -P --delete "$source" "$user"@"$server":"$target"