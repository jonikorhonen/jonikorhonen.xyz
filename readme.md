# jonikorhonen.xyz

Git repo for my personal website at https://jonikorhonen.xyz 

## Requirements

- pandoc
- python3
  - pyyaml
  - pypandoc

## Building

Running `./build.py` copies assets to `./dist` and runs pandoc for content markdown files placing produced html files to `./dist`.

Copying and assembling is only done if needed because of content update or template update.

## Running locally

Running `./run-local.sh` launches python http server as module and serves content from `./dist` at `http://localhost:9000`

## Deploying

Running `./deploy.sh` deploys contents from `./dist` to server.