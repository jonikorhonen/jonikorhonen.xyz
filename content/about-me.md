---
title: About me
date: 2021-11-26
---

I'm a programmer with over a decade under my belt and currently, I work as full stack software engineer mainly with
Java, Node.js and React with CI/CD technologies differing project to project.

On my free time I like to take easy by reading, going for walk or running, and some tinkering with whatever manages to catch
my interest.

You can contact me through email at joni - at - jonikorhonen - dot - xyz