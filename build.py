#!/usr/bin/env python3
import datetime
import glob
import json
import os
import re
import shutil

import pypandoc
import yaml

main_dir = os.path.abspath(os.path.dirname(__file__))
assets_dir = os.path.join(main_dir, "assets")
content_dir = os.path.join(main_dir, "content")
posts_dir = os.path.join(content_dir, "posts")
templates_dir = os.path.join(main_dir, "templates")

build_dir = os.path.join(main_dir, "build")
dist_dir = os.path.join(main_dir, "dist")

dist_style_dir = os.path.join(dist_dir, "styles")
dist_style_file = os.path.join(dist_style_dir, "style.min.css")

dist_script_dir = os.path.join(dist_dir, "scripts")
dist_script_file = os.path.join(dist_script_dir, "main.js")

dist_images_dir = os.path.join(dist_dir, "images")

dist_posts_dir = os.path.join(dist_dir, "posts")


def create_dist_hierarchy():
    directories = [
        build_dir,
        dist_style_dir,
        dist_script_dir,
        dist_posts_dir,
        dist_images_dir,
    ]
    for directory in directories:
        os.makedirs(directory, exist_ok=True)


def source_is_newer(source, target):
    if os.path.exists(target):
        return os.path.getmtime(source) > os.path.getmtime(target)
    return True


def newest_from_directory(directory, ext=""):
    return max(glob.iglob(directory + "/*" + ext), key=os.path.getmtime)


def should_build_assets_from_source(source_dir, target, asset_ext):
    return not os.path.exists(target) or source_is_newer(newest_from_directory(source_dir, ext=asset_ext), target)


def absolute_file_paths(directory):
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))


# For directory of assets which should be concatenated to single file
def concat_assets_of_type(asset_type, source_dir, target_file, asset_ext="", write_to_target=True):
    if should_build_assets_from_source(source_dir, target_file, asset_ext):
        print("  %s built" % asset_type)
        with open(target_file, "w") as outfile:
            outstring = ""
            for file in sorted(absolute_file_paths(source_dir)):
                with open(file) as infile:
                    for line in infile:
                        if write_to_target:
                            outfile.write(line)
                        else:
                            outstring += line + "\n"
            if not write_to_target:
                return outstring
            else:
                return None
    else:
        print("  %s up to date, skipping" % asset_type)
        return None


# From https://stackoverflow.com/a/223689
def minimize_css(css: str) -> str:
    # remove comments - this will break a lot of hacks :-P
    css = re.sub(r'\s*/\*\s*\*/', "$$HACK1$$", css)  # preserve IE<6 comment hack
    css = re.sub(r'/\*[\s\S]*?\*/', "", css)
    css = css.replace("$$HACK1$$", '/**/')  # preserve IE<6 comment hack

    # url() doesn't need quotes
    css = re.sub(r'url\((["\'])([^)]*)\1\)', r'url(\2)', css)

    # spaces may be safely collapsed as generated content will collapse them anyway
    css = re.sub(r'\s+', ' ', css)

    # shorten collapsable colors: #aabbcc to #abc
    css = re.sub(r'#([0-9a-f])\1([0-9a-f])\2([0-9a-f])\3(\s|;)', r'#\1\2\3\4', css)

    # fragment values can loose zeros
    css = re.sub(r':\s*0(\.\d+([cm]m|e[mx]|in|p[ctx]))\s*;', r':\1;', css)

    output = ""
    for rule in re.findall(r'([^{]+){([^}]*)}', css):

        # we don't need spaces around operators
        selectors = [re.sub(r'(?<=[\[(>+=])\s+|\s+(?=[=~^$*|>+\])])', r'', selector.strip()) for selector in
                     rule[0].split(',')]

        # order is important, but we still want to discard repetitions
        properties = {}
        porder = []
        for prop in re.findall('(.*?):(.*?)(;|$)', rule[1]):
            key = prop[0].strip().lower()
            if key not in porder:
                porder.append(key)
            properties[key] = prop[1].strip()

        # output rule if it contains any declarations
        if properties:
            output += "%s{%s}" % (
                ','.join(selectors), ''.join(['%s:%s;' % (key, properties[key]) for key in porder])[:-1])

    return output


# For single file assets
def copy_asset(source, target):
    if os.path.exists(target) and not source_is_newer(source, target):
        print("  %s up to date, skipping" % os.path.split(source)[1])
    else:
        print("  %s" % os.path.split(source)[1])
        shutil.copyfile(source, target)


def copy_images():
    images_copied = False
    images_dir = os.path.join(assets_dir, "images")
    image_extensions = [".jpg", ".png", ".gif", ".svg"]
    image_list = []
    for file in absolute_file_paths(images_dir):
        if os.path.splitext(file)[1] in image_extensions:
            image_list.append(file)
    for image in image_list:
       image_name = os.path.split(image)[1]
       copy_asset(image, os.path.join(dist_images_dir, image_name))

    if len(image_list) == 0:
        print("  no images to copy")


def build_assets():
    print("--Assets")
    css = concat_assets_of_type("Styles",
                                os.path.join(assets_dir, "css"),
                                dist_style_file,
                                asset_ext=".css",
                                write_to_target=False)
    if css is not None:
        css = minimize_css(css)
        with open(dist_style_file, 'w') as css_file:
            css_file.write(css)
    concat_assets_of_type("Scripts", os.path.join(assets_dir, "script"), dist_script_file, asset_ext=".js")
    copy_images()
    copy_asset(os.path.join(assets_dir, "favicon.ico"), os.path.join(dist_dir, "favicon.ico"))
    copy_asset(os.path.join(assets_dir, "robots.txt"), os.path.join(dist_dir, "robots.txt"))


pandoc_includes_args = [
    "--standalone",
    "--metadata-file=" + os.path.join(main_dir, "pandoc-metadata.json"),
    "--metadata=title:\" \""
]


def build_includes():
    print("--Includes")
    for item in ["header", "footer"]:
        template = os.path.join(templates_dir, item + "-template.html")
        outputfile = os.path.join(build_dir, item + ".html")
        if source_is_newer(template, outputfile):
            print("  %s" % item)
            arguments = pandoc_includes_args
            arguments.append("--template=" + template)
            pypandoc.convert_text("",
                                  format="md",
                                  to="html5",
                                  outputfile=outputfile,
                                  extra_args=arguments)
        else:
            print("  %s up to date, skipping" % item)


pandoc_args = [
    "--standalone",
    "--metadata-file=" + os.path.join(main_dir, "pandoc-metadata.json"),
    "--include-before-body=" + os.path.join(build_dir, "header.html"),
    "--include-after-body=" + os.path.join(build_dir, "footer.html"),
    "--lua-filter=" + os.path.join(main_dir, "pandoc-filters.lua")
]


def run_pandoc(source, path, template, extra_arguments=None):
    if extra_arguments is None:
        extra_arguments = []
    arguments = pandoc_args.copy()
    template_path = os.path.join(templates_dir, template + "-template.html")
    arguments.append("--template=" + template_path)
    arguments.append("--metadata=path:" + path)
    arguments += extra_arguments
    outputfile = os.path.join(dist_dir, path)
    pypandoc.convert_file(source, format="md", to="html5", outputfile=outputfile, extra_args=arguments)


def get_post_metadata(post):
    result = ""
    with open(post, 'r') as fp:
        for line in re.findall('---(.*?)---', fp.read(), re.S):
            result += line + "\n"
    return yaml.safe_load(result.strip())


class DateEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()


def content_needs_update(page=None, template="must-provide", source=None, target=None):
    if source is None:
        source = os.path.join(content_dir, page + ".md")
    if target is None:
        target = os.path.join(dist_dir, page + ".html")
    header_include = os.path.join(build_dir, "header.html")
    footer_include = os.path.join(build_dir, "footer.html")
    template_file = os.path.join(templates_dir, template + "-template.html")

    if os.path.exists(target):
        return source_is_newer(header_include, target) \
               or source_is_newer(footer_include, target) \
               or source_is_newer(template_file, target) \
               or source_is_newer(source, target)
    return True


def build_posts():
    print("--Posts")
    metadata = []
    has_changes = False
    for post in absolute_file_paths(posts_dir):
        post_name = os.path.splitext(os.path.split(post)[1])[0]
        post_metadata = get_post_metadata(post)
        post_metadata['url'] = "/posts/" + post_name + ".html"
        metadata.append(post_metadata)
        if content_needs_update("posts/" + post_name, "content-page"):
            print("  %s" % post_name)
            run_pandoc(post, "posts/" + post_name + ".html", "content-page")
            has_changes = True
        else:
            print("  %s up to date, skipping" % post_name)

    metadata.sort(key=lambda item: item.get("date"), reverse=True)
    posts_target = os.path.join(dist_dir, "posts", "index.html")
    posts_source = os.path.join(content_dir, "posts.md")
    if has_changes or content_needs_update(template="posts-page", source=posts_source, target=posts_target):
        posts_meta = os.path.join(build_dir, "posts.json")
        recent_meta = os.path.join(build_dir, "recent-posts.json")
        with open(posts_meta, 'w') as file:
            json.dump({"posts": metadata}, file, indent=2, cls=DateEncoder)
        with open(recent_meta, 'w') as file:
            json.dump({"posts": metadata[:5]}, file, indent=2, cls=DateEncoder)
        print("  posts")
        run_pandoc(posts_source, "posts/index.html", "posts-page", ["--metadata-file=" + posts_meta])
    return has_changes


def build_frontpage(force_update):
    print("--Frontpage")
    if content_needs_update("index", "index") or force_update:
        print("  index")
        run_pandoc(os.path.join(content_dir, "index.md"), "index.html", "index",
                   ["--metadata-file=" + os.path.join(build_dir, "recent-posts.json")])
    else:
        print("  index up to date, skipping")


def build_content_pages():
    print("--Content pages")
    for page in ["about-me"]:
        if content_needs_update(page, "content-page"):
            print("  %s" % page)
            source = os.path.join(content_dir, page + ".md")
            run_pandoc(source, page + ".html", "content-page")
        else:
            print("  %s up to date, skipping" % page)


if __name__ == "__main__":
    print("Building release files to %s" % dist_dir)
    create_dist_hierarchy()
    build_assets()
    build_includes()
    posts_updated = build_posts()
    build_frontpage(posts_updated)
    build_content_pages()
