#!/usr/bin/env sh

set -e

SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")

echo "Running local development server"

cd "$DIR"/dist

python -m http.server 9000